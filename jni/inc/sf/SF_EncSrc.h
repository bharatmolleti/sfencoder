#ifndef SF_ENCODER_SOURCE
#define SF_ENCODER_SOURCE

#include <pthread.h>
#include "media/stagefright/MediaSource.h"
#include "media/stagefright/MediaBufferGroup.h"

#define SRC_MAX_BUFFER_CHUNK  (8 * 1024 * 1024)
#define SRC_QUEUE_SIZE (2)

using namespace android;

class SFESource : public MediaSource
{
	public:
		SFESource (int width, int height, int fps, int colorFormat);

		virtual sp<MetaData> getFormat ();

		virtual status_t start (MetaData *params);

		virtual status_t stop ();

		virtual status_t read (MediaBuffer **buffer, const MediaSource::ReadOptions *options);

		int putBuffer(void** pBuffer);


	protected:
	    virtual ~SFESource();

	private:
		MediaBufferGroup mGroup;
		int mWidth, mHeight;    
		int mMaxNumFrames;    
		int mFrameRate;    
		int mColorFormat;    
		size_t mSize;
		int64_t mNumFramesOutput;
		int64_t mNumFramesInput;

		int mnCurrentReadBuffer;
		int mnCurrentWriteBuffer;
		void** mpDataBuffer;
		int* mnDataBuffer;
		int nDataBuffer;
		volatile bool mIsBufferReady;

		bool mEOFReached;
		
		SFESource(const SFESource &);
		
		SFESource &operator=(const SFESource &);		
		
		pthread_mutex_t mutex;
		pthread_cond_t cond_full;
		pthread_cond_t cond_empty;
};

#endif	// SF_ENCODER_SOURCE
