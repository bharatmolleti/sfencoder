#ifndef SF_ENCODER_PRIV
#define SF_ENCODER_PRIV

#include "media/stagefright/OMXClient.h"
#include "media/stagefright/MediaSource.h"

using namespace android;

typedef struct
{
	sp<MediaSource> encoder;
	OMXClient client;
}SFEncoderPrivate;

#endif	// SF_ENCODER_PRIV