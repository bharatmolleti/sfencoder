#ifndef SF_ENCODER_SINK
#define SF_ENCODER_SINK

#include <pthread.h>
#include "media/stagefright/MediaSource.h"
#include "media/stagefright/foundation/ABase.h"

#define SINK_MAX_BUFFER_CHUNK  (8 * 1024 * 1024)
#define SINK_QUEUE_SIZE (2)

using namespace android;

class SFESink
{
	public:
		
		SFESink (const sp<MediaSource> &source);

		~SFESink ();
		
		status_t start ();
		status_t stop ();

		bool reachedEOS();

		int getBuffer(void** pBuffer, int* nSize);

	private:
		sp <MediaSource> mSource;
		bool mStarted;
		pthread_t mThread;

		volatile bool mStopped;

		int mnCurrentReadBuffer;
		int mnCurrentWriteBuffer;
		void** mpDataBuffer;
		int* mnDataBuffer;
		int nDataBuffer;
		volatile bool mIsBufferReady;
		
		pthread_mutex_t mutex;
		pthread_cond_t cond_full;
		pthread_cond_t cond_empty;

	    static void *threadWrapper(void *pthis);

		 DISALLOW_EVIL_CONSTRUCTORS(SFESink);

		 void readFromSource();
};

#endif	// SF_ENCODER_SINK
