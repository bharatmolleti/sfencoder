#ifndef SF_ENCODER
#define SF_ENCODER

typedef enum
{
	SFE_COLOR_Format_QCOM_NV12	
}SFEncoder_ColorFormat;

typedef enum
{
	SFE_MEDIA_MIMETYPE_VIDEO_AVC = 0
}SFEncoder_Codec;

typedef enum
{
	SFE_VIDEO_AVCProfileBaseline,
	SFE_VIDEO_AVCProfileHigh
}SFEncoder_Profile;

typedef enum
{
	SFE_VIDEO_AVCLevel4
}SFEncoder_Level;

typedef struct
{
	int nFps;
	int nWidth;
	int nHeight;
	int nBitrate;
	int nIFrameSeconds;	// time between iframe in seconds.
	SFEncoder_Codec tCodec;
	SFEncoder_Level tLevel;
	SFEncoder_Profile tProfile;
	SFEncoder_ColorFormat tColorFormat;
}SFEncoderInitParams;

class SFEncoder
{
	public:
		SFEncoder ();
		~SFEncoder ();
		
		int init (SFEncoderInitParams& params);

		// this method will lock
		int encodeFrame (void** pBuffer);

		int getFrame (void** pFrame, int* nSize);

		int close ();		
	
	private:		
		
		void* mpSource;
		void* mpSink;
		void* mpSF;

		int mnFrameRateFps;
		int mnWidth;
		int mnHeight;
		int mnBitRateBps;
		int mnIFramesIntervalSeconds;
		int mnColorFormat;
		int mnLevel;
		int mnProfile;
		int mnCodec;
};

#endif	// SF_ENCODER