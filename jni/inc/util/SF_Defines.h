#ifndef SF_DEFINES
#define SF_DEFINES

#include <android/log.h>

#define SFE_TAG "SFEncoder"

#define SFE_LOG(...) __android_log_print(ANDROID_LOG_INFO, SFE_TAG, __VA_ARGS__)

typedef enum
{
	SF_OK = 0, SF_BUSY = 1, SF_ERROR = 2, SF_EOF = 3, SF_MEMORY = 4
}SEEncoder_Result;

#endif // SF_DEFINES