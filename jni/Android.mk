LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE    := sfencoder

LOCAL_CFLAGS += -Wno-multichar

LOCAL_C_INCLUDES :=  \
	$(LOCAL_PATH)/inc \
	$(LOCAL_PATH)/inc/sf \
	$(LOCAL_PATH)/inc/util \
	$(LOCAL_PATH)/include \
	$(LOCAL_PATH)/include/openmax

LOCAL_SRC_FILES := 	\
					src/sf/SF_EncSrc.cpp \
					src/sf/SF_EncSnk.cpp \
					src/sf/SF_Encoder.cpp \
					src/test/main.cpp

LOCAL_LDLIBS    := -lstagefright -lmedia -lutils -llog -lbinder -lstagefright_foundation

include $(BUILD_EXECUTABLE)