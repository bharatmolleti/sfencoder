#include "SF_EncSrc.h"
#include "media/stagefright/MetaData.h"
#include "media/stagefright/MediaDefs.h"

#include "SF_Defines.h"

using namespace android;

SFESource::SFESource (int width, int height, int fps, int colorFormat)
	: mWidth(width),
	mHeight(height),
	mFrameRate(fps),
	mColorFormat(colorFormat),
	mEOFReached (false),
	mSize((width * height * 3) / 2), 
	mIsBufferReady(false),
	mpDataBuffer(NULL),
	mnDataBuffer(0),
	mnCurrentReadBuffer (0),
	mnCurrentWriteBuffer (0)
{
	mGroup.add_buffer (new MediaBuffer (mSize));

	mnDataBuffer = (int*) malloc(SRC_QUEUE_SIZE * sizeof(int));
	mpDataBuffer = (void**) malloc(SRC_QUEUE_SIZE * sizeof(void*));
	for (int i = 0; i < SRC_QUEUE_SIZE; i++)
	{
		mpDataBuffer[i] = malloc(SRC_MAX_BUFFER_CHUNK);
	}

	pthread_mutex_init(&mutex, NULL);
	pthread_cond_init(&cond_full, NULL);
	pthread_cond_init(&cond_empty, NULL);
}

SFESource::~SFESource ()
{
	if(mpDataBuffer)
	{
		for (int i = 0; i < SRC_QUEUE_SIZE; i++)
		{
			if(mpDataBuffer[i])
			{
				free (mpDataBuffer[i]);
			}
		}
		free (mpDataBuffer);
		mpDataBuffer = NULL;
	}

	if (mnDataBuffer)
	{
		free (mnDataBuffer);
		mnDataBuffer = NULL;
	}
	
	pthread_mutex_destroy(&mutex);
	pthread_cond_destroy(&cond_full);
	pthread_cond_destroy(&cond_empty);
}

sp<MetaData> SFESource::getFormat ()
{
	sp<MetaData> meta = new MetaData;
	meta->setInt32 (kKeyWidth, mWidth);
	meta->setInt32 (kKeyHeight, mHeight);
	meta->setInt32 (kKeyColorFormat, mColorFormat);
	meta->setCString (kKeyMIMEType, MEDIA_MIMETYPE_VIDEO_RAW);
	return meta;
}

status_t SFESource::start (MetaData* params)
{
	mNumFramesOutput = 0;
	mNumFramesInput = 0;
	return OK;
}

status_t SFESource::stop ()
{
	return OK;
}

status_t SFESource::read (MediaBuffer** buffer, const MediaSource::ReadOptions* options)
{
	SFE_LOG("\n%s Wait", __func__);
	pthread_mutex_lock(&(mutex));
	SFE_LOG("\n%s Obtained", __func__);

	while (mIsBufferReady == false) {
		 SFE_LOG("%s Cond Wait", __func__);
		pthread_cond_wait(&(cond_empty), &(mutex));
	}

	SFE_LOG("\n%s Cond Obtained", __func__);
	
	status_t err = mGroup.acquire_buffer (buffer);
	if (err != OK)
	{
		pthread_mutex_unlock(&(mutex));
		pthread_cond_broadcast(&(cond_full));
		return err;
	}

	
	if (mnCurrentReadBuffer >= SRC_QUEUE_SIZE)
	{
		mnCurrentReadBuffer = 0;
	}	

	if(mnDataBuffer[mnCurrentReadBuffer] == 0)
	{
		SFE_LOG(" %s Sending in EOF.", __func__);
		err = ERROR_END_OF_STREAM;
		mEOFReached = true;
	}
	else
	{
		memcpy ((*buffer)->data(), mpDataBuffer[mnCurrentReadBuffer], mSize);
		(*buffer)->set_range (0, mSize);
		(*buffer)->meta_data ()->clear ();
		(*buffer)->meta_data ()->setInt64 (kKeyTime, (mNumFramesOutput * 1000000) / mFrameRate);
		++mNumFramesOutput;
	}

	mnCurrentReadBuffer++;

	mIsBufferReady = false;
	
	SFE_LOG("\n %s  Unlock and Broadcast ", __func__);
	pthread_mutex_unlock(&(mutex));
	pthread_cond_broadcast(&(cond_full));
	
	return err;
}

int SFESource::putBuffer(void** pBuffer)
{
	SFE_LOG("\n%s Wait", __func__);
	pthread_mutex_lock(&(mutex));
	SFE_LOG("\n%s Obtained", __func__);

	if (mIsBufferReady == true)
	{
		SFE_LOG("\n%s Busy", __func__);
		pthread_mutex_unlock(&(mutex));
		return SF_BUSY;
	}

	while(mIsBufferReady == true)
	{			
		 SFE_LOG("\n%s Cond Wait", __func__);
		 pthread_cond_wait(&(cond_full), &(mutex));
	}
	
	SFE_LOG("\n%s Cond Obtained", __func__);
	
	if (mnCurrentWriteBuffer >= SRC_QUEUE_SIZE)
	{
		mnCurrentWriteBuffer = 0;
	}
	
	mNumFramesInput++;

	if(*pBuffer == NULL)
	{
		SFE_LOG ("%s User gave EOF!", __func__);
		mnDataBuffer[mnCurrentWriteBuffer] = 0;
	}
	else
	{
		memcpy (mpDataBuffer[mnCurrentWriteBuffer], *pBuffer , mSize);
		mnDataBuffer[mnCurrentWriteBuffer] = mSize;
	}
	
	mnCurrentWriteBuffer++;	
	mIsBufferReady = true;

	SFE_LOG("\n %s  Unlock and Broadcast ", __func__);	
	pthread_mutex_unlock(&(mutex));
	pthread_cond_broadcast(&(cond_empty));
	
	return OK;
}
