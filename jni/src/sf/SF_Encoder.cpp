#include "SF_Encoder.h"
#include "binder/ProcessState.h"
#include "media/stagefright/OMXCodec.h"
#include "media/stagefright/foundation/ADebug.h"

#include "media/stagefright/MetaData.h"
#include "media/stagefright/MediaDefs.h"

#include "SF_Encoder_Priv.h"
#include "SF_Defines.h"

#include "SF_EncSrc.h"
#include "SF_EncSnk.h"

SFEncoder::SFEncoder ()
	: 	mpSF (NULL), 
		mpSource (NULL), 
		mpSink (NULL)
{
}

SFEncoder::~SFEncoder ()
{
	close();
}

int SFEncoder::init (SFEncoderInitParams& params)
{
	mpSF = new SFEncoder();
	if(mpSF == NULL)
	{
		SFE_LOG ("%s Memory failure for OMX Objects", __func__);
		return SF_MEMORY;
	}
	
	SFEncoderPrivate* sfComp = (SFEncoderPrivate*) mpSF;

	mnFrameRateFps = params.nFps;
	mnWidth = params.nWidth;
	mnHeight = params.nHeight;
	mnBitRateBps = params.nBitrate;
	mnIFramesIntervalSeconds = params.nIFrameSeconds;

	if(params.tColorFormat == SFE_COLOR_Format_QCOM_NV12)
	{
		mnColorFormat = OMX_QCOM_COLOR_FormatYUV420PackedSemiPlanar64x32Tile2m8ka;
	}
	else
	{
		SFE_LOG ("%s Unsupported Color Format : %d ", __func__, params.tColorFormat);
		return SF_ERROR;
	}

	if(params.tLevel == SFE_VIDEO_AVCLevel4)
	{
		mnLevel = OMX_VIDEO_AVCLevel4;
	}
	else
	{
		SFE_LOG ("%s Unsupported Level %d, using default %d", __func__, params.tLevel, SFE_VIDEO_AVCLevel4);
	}

	if(params.tProfile == SFE_VIDEO_AVCProfileBaseline)
	{
		mnProfile = OMX_VIDEO_AVCProfileBaseline;
	}	
	else if(params.tProfile == SFE_VIDEO_AVCProfileHigh)
	{
		mnProfile = OMX_VIDEO_AVCProfileHigh;
	}
	else
	{
		SFE_LOG ("%s Unsupported Profile %d", __func__, params.tProfile);
		return SF_ERROR;
	}

	if(params.tCodec == SFE_MEDIA_MIMETYPE_VIDEO_AVC)
	{
		mnCodec = 0;
	}
	else
	{
		SFE_LOG ("%s Unsupported Codec %d", __func__, params.tCodec);
		return SF_ERROR;
	}

	android::ProcessState::self()->startThreadPool();
	
	CHECK_EQ(sfComp->client.connect(), (status_t)OK);

	status_t err = OK;
		
	SFESource* pSource = new SFESource(mnWidth, mnHeight, mnFrameRateFps, mnColorFormat);
	mpSource = (void*)pSource;		

	sp<MetaData> enc_meta = new MetaData;
	switch (mnCodec) 
	{        
		case 1:
			enc_meta->setCString(kKeyMIMEType, MEDIA_MIMETYPE_VIDEO_MPEG4);
			break;

		case 2:
			enc_meta->setCString(kKeyMIMEType, MEDIA_MIMETYPE_VIDEO_H263);
			break;

		default:
			enc_meta->setCString(kKeyMIMEType, MEDIA_MIMETYPE_VIDEO_AVC);
			break;
	}

	enc_meta->setInt32 (kKeyWidth, mnWidth);
	enc_meta->setInt32 (kKeyHeight, mnHeight);
	enc_meta->setInt32 (kKeyFrameRate, mnFrameRateFps);
	enc_meta->setInt32 (kKeyBitRate, mnBitRateBps);
	enc_meta->setInt32 (kKeyStride, mnWidth);
	enc_meta->setInt32 (kKeySliceHeight, mnHeight);
	enc_meta->setInt32 (kKeyIFramesInterval, mnIFramesIntervalSeconds);

	if (mnLevel != -1)
	{
		SFE_LOG ("%s MetaData Level Set to %d", __func__, mnLevel);
		enc_meta->setInt32 (kKeyVideoLevel, mnLevel);
	}

	if (mnProfile != -1)
	{		
		SFE_LOG("%s MetaData profile Set to %d", __func__,  mnProfile);
		enc_meta->setInt32(kKeyVideoProfile, mnProfile);
	}

	SFE_LOG("%s MetaData Params Set ", __func__);

	sfComp->encoder =	OMXCodec::Create(								
				sfComp->client.interface(),
				enc_meta, true /* createEncoder */,
				pSource, 							
				NULL,
				0,
				NULL);

	if(sfComp->encoder == NULL)
	{		
		SFE_LOG("%s Creating Encoder with these options failed.. try a new set", __func__);
		return SF_ERROR;
	}

	SFESink* pSink = new SFESink(sfComp->encoder);
	mpSink = (void*) pSink;
	
	pSink->start();
	
	SFE_LOG("%s : Encoder Operational", __func__);
	return SF_OK;
}

int SFEncoder::encodeFrame (void** pBuffer)
{
	SFESource* pSource = (SFESource*) mpSource;
	int nStatus = pSource->putBuffer (pBuffer);	
	return nStatus;
}

int SFEncoder::getFrame (void** pFrame, int* nSize)
{	
	SFESink* pSink = (SFESink*) mpSink;
	int nStatus = pSink->getBuffer(pFrame, nSize);	
	if((nStatus == SF_BUSY) && (pSink->reachedEOS() == true))
	{
		return SF_EOF;
	}
	return nStatus;
}

int SFEncoder::close ()
{	
	if (mpSink)
	{
		SFESink* pSink = (SFESink*) mpSink;
		pSink->stop();
		delete pSink;
		mpSink = NULL;
	}

	if (mpSource)
	{
		mpSource = NULL;
	}

	if(mpSF)
	{
		SFEncoderPrivate* sfComp = (SFEncoderPrivate*) mpSF;
		sfComp->client.disconnect();
		//delete sfComp; // crashes if enabled. sp<> apparently cleaned itself up.?
		mpSF = NULL;
	}
	return SF_OK;
}
