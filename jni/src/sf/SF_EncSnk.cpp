#include "SF_EncSnk.h"
#include "media/stagefright/MediaBuffer.h"

#include "SF_Defines.h"

void* SFESink::threadWrapper(void * pthis)
{
	 SFE_LOG("SFESink::Thread: %p", pthis);
	 SFESink *writer = static_cast<SFESink *>(pthis);
	 writer->readFromSource();
	 return NULL;
}

SFESink::SFESink (const sp<MediaSource> &source)
	: mSource(source),
	mStarted(false),
	mStopped(false),
	mIsBufferReady(false),
	mpDataBuffer(NULL),
	mnDataBuffer(0),
	mnCurrentReadBuffer (0), 
	mnCurrentWriteBuffer (0)
{
	mnDataBuffer = (int*) malloc(SINK_QUEUE_SIZE * sizeof(int));
	mpDataBuffer = (void**) malloc(SINK_QUEUE_SIZE * sizeof(void*));
	for (int i = 0; i < SINK_QUEUE_SIZE; i++)
	{
		mpDataBuffer[i] = malloc(SINK_MAX_BUFFER_CHUNK);
	}
	
	pthread_mutex_init(&mutex, NULL);
	pthread_cond_init(&cond_full, NULL);
	pthread_cond_init(&cond_empty, NULL);
}

SFESink::~SFESink ()
{
	if(mpDataBuffer)
	{
		for (int i = 0; i < SINK_QUEUE_SIZE; i++)
		{
			if(mpDataBuffer[i])
			{
				free (mpDataBuffer[i]);
			}
		}
		free (mpDataBuffer);
		mpDataBuffer = NULL;
	}

	if (mnDataBuffer)
	{
		free (mnDataBuffer);
		mnDataBuffer = NULL;
	}
	
	pthread_mutex_destroy(&mutex);
	pthread_cond_destroy(&cond_full);
	pthread_cond_destroy(&cond_empty);
}

status_t SFESink::start ()
{
	if(mpDataBuffer == NULL)
	{
		SFE_LOG("\n%s Memory Allocation Error", __func__);
		return NO_MEMORY;
	}
	
	mStarted = true;

	mSource->start ();

	pthread_attr_t attr;
	pthread_attr_init (&attr);
	pthread_attr_setdetachstate (&attr, PTHREAD_CREATE_JOINABLE);
	int err = pthread_create (&mThread, &attr, threadWrapper, this);
	pthread_attr_destroy (&attr);
	
	if (err) 
	{        
		SFE_LOG("%s Error creating thread!", __func__);
		return -ENODEV;
	}
	
	return OK;
}

status_t SFESink::stop ()
{
	mStarted = false;	

	mSource->stop();
	
	void *dummy;
	pthread_join(mThread, &dummy);
	status_t err = static_cast<status_t>(reinterpret_cast<uintptr_t>(dummy));

	SFE_LOG ("%s Ending the reading thread", __func__);
	return err;
}

bool SFESink::reachedEOS ()
{
	return mStopped;
}

void SFESink::readFromSource ()
{
	if (!mStarted) 
	{
		return;
	}

	status_t err = OK;
	MediaBuffer *buffer;

	while (mStarted && (err = mSource->read (&buffer)) == OK)
	{
		SFE_LOG("\n%s Wait", __func__);
		pthread_mutex_lock(&(mutex));
		SFE_LOG("\n%s Obtained", __func__);

		while(mIsBufferReady == true)
		{			
			 SFE_LOG("\n%s Cond Wait", __func__);
			 pthread_cond_wait(&(cond_full), &(mutex));
		}
		
		SFE_LOG("\n%s Cond Obtained", __func__);
		
		if (buffer == NULL)
		{
			SFE_LOG ("\n%s Obtained EOF", __func__);
			mStopped = true;
			return;
		}
		
		nDataBuffer = buffer->range_length ();

		SFE_LOG("\n%s Encoder has given a buffer of size : %d ", __func__, buffer->range_length ());

		if (mnCurrentReadBuffer >= SINK_QUEUE_SIZE)
		{
			mnCurrentReadBuffer = 0;
		}
		
		memcpy (mpDataBuffer[mnCurrentReadBuffer], buffer->data (), nDataBuffer);
		mnDataBuffer[mnCurrentReadBuffer] = nDataBuffer;	

		mnCurrentReadBuffer++;
		mIsBufferReady = true;

		SFE_LOG("\n%s Before Unlock and Broadcast", __func__);
				
		pthread_mutex_unlock(&(mutex));
		pthread_cond_broadcast(&(cond_empty));

		buffer->release ();
		buffer = NULL;
		
		SFE_LOG("\n %s  Unlock and Broadcast ", __func__);
		fflush (stdin);
		SFE_LOG ("\n %s  Unlock and Broadcast ", __func__);
		usleep(100);
	}

	SFE_LOG("\n%s Obtained Signal : %s (%d)", __func__, (err==ERROR_END_OF_STREAM)?"EOF":"Unknown", err);
	
	mStopped = true;
}

int SFESink::getBuffer(void** pBuffer, int* nSize)
{
	SFE_LOG("\n%s Wait", __func__);
	pthread_mutex_lock(&(mutex));
	SFE_LOG("\n%s Obtained", __func__);

	if (mIsBufferReady == false)
	{
		SFE_LOG("\n%s Busy", __func__);
		pthread_mutex_unlock(&(mutex));
		return SF_BUSY;
	}
	
	while (mIsBufferReady == false) {
		 SFE_LOG("%s Cond Wait", __func__);
		pthread_cond_wait(&(cond_empty), &(mutex));
	}

	SFE_LOG("\n%s Cond Obtained", __func__);
	if (mnCurrentWriteBuffer >= SINK_QUEUE_SIZE)
	{
		mnCurrentWriteBuffer = 0;
	}
	
	*pBuffer = mpDataBuffer[mnCurrentWriteBuffer];
	*nSize = mnDataBuffer[mnCurrentWriteBuffer];

	mnCurrentWriteBuffer++;
	
	mIsBufferReady = false;

	pthread_mutex_unlock(&(mutex));
	pthread_cond_broadcast(&(cond_full));

	SFE_LOG("\n %s  Unlock and Broadcast ", __func__);
	
	return SF_OK;
}
