#include <cstdio>
#include <cstdlib>
#include <unistd.h>
#include "SF_Encoder.h"
#include "SF_Defines.h"

const int WIDTH = 1280;
const int HEIGHT = 720;
const int FPS = 30;
const int ENCODE_DURATION_SEC = 8;
const int FRAMES_TO_ENCODE = FPS*ENCODE_DURATION_SEC;

const char* sInputFile = "/sdcard/test720pnv12.yuv";
const char* sOutputFile = "/sdcard/test720p.h264";

int main()
{
	printf("\n Test has started");
	
	SFEncoder* encoder = new SFEncoder ();
	
	SFEncoderInitParams initParams;
	
	initParams.nFps = FPS;
	initParams.nWidth = WIDTH;
	initParams.nHeight = HEIGHT;
	initParams.nBitrate = WIDTH*HEIGHT*8;
	initParams.nIFrameSeconds = 1;
	
	initParams.tCodec = SFE_MEDIA_MIMETYPE_VIDEO_AVC;
	initParams.tLevel = SFE_VIDEO_AVCLevel4;
	initParams.tProfile = SFE_VIDEO_AVCProfileHigh;
	initParams.tColorFormat = SFE_COLOR_Format_QCOM_NV12;
	
	if(SF_OK != encoder->init (initParams))
	{
		printf ("\n Initialization Failed.. ");
		return 0;
	}

	void* pSource = malloc (initParams.nWidth*initParams.nHeight*2);
	void* pBuffer = pSource;
	if(pBuffer == NULL)
	{
		printf ("\n Insufficient Memory.. ");
		return 0;
	}
	
	int nInFrames = 0;
	int nOutFrames = 0;
	int nEOFReached = 0;
	int nEOFLoopsAllowed = 8;
	
	void* pFrame = NULL;
	int nSize = 0;
	
	int nYUVBufferSize = (int)((double)initParams.nWidth*(double)initParams.nHeight*(double)1.5);
	FILE* fp = fopen(sInputFile, "rb");
	if(fp == NULL)
	{
		printf ("\n %s File could'nt be loaded.", sInputFile);			
	}
	else
	{
		if (nInFrames == 0)
		{
			fread (pBuffer, 1, nYUVBufferSize, fp);
		}
	}
	
	FILE* fout = fopen (sOutputFile, "wb");
	if (fout == NULL)
	{
		printf ("\n %s Output File could'nt be loaded.", sOutputFile);		
	}

	do
	{		
		if (SF_OK == encoder->encodeFrame (&pBuffer))
		{			
			if (nInFrames > FRAMES_TO_ENCODE)
			{
				printf ("\n EOF has been reached.");
				nEOFReached = 1;
				pBuffer = NULL;
			}
			else
			{
				nInFrames++;
				if(fp != NULL)
				{
					if (feof (fp))
					{
						fseek (fp, 0, SEEK_SET);
					}					
					fread (pBuffer, 1, nYUVBufferSize, fp);
				}
			}
		}

		int nEncStatus = encoder->getFrame (&pFrame, &nSize);
		if (SF_OK == nEncStatus)
		{
			printf("\n %d - %d", nInFrames, nOutFrames);
			printf("\n Encoder gave a frame (%p) of size (%d)", pFrame, nSize);			
			nOutFrames ++;
			if (fout != NULL)
			{				
				fwrite (pFrame, 1, nSize, fout);
			}
			
			if(nEOFReached == 1)
			{
				if(nOutFrames == nInFrames)
				{
					printf("\n All the needed frames have been encoded.");
					break;
				}				
			}
		}
		else
		{
			if(3 == nEncStatus)
			{
				printf("\n Encoder Reached EOS");
				break;
			}
		}
		
		usleep(100);		
	}while (1);

	printf("\n %d - %d", nInFrames, nOutFrames);
	encoder->close ();
	
	free (pSource);
	pSource = NULL;
	
	delete encoder;
	encoder = NULL;
	
	if (fp)
	{
		fclose (fp);
		fp = NULL;
	}
	
	if (fout)
	{
		fclose (fout);
		fout = NULL;
	}
	
	printf("\n Test has ended");
	return 0;
}